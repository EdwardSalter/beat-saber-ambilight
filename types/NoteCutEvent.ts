type NoteCutDirection =
  | "Up"
  | "Down"
  | "Left"
  | "Right"
  | "UpLeft"
  | "UpRight"
  | "DownLeft"
  | "DownRight"
  | "Any"
  | "None"; // Direction the note is supposed to be cut in

export type NoteType = "NoteA" | "NoteB" | "Bomb";

export interface NoteCutEvent {
  event: "noteCut";
  noteCut: {
    noteType: NoteType; // Type of note
    noteCutDirection: NoteCutDirection;
    noteLine: number; // The horizontal position of the note, from left to right [0..3]
    noteLayer: number; // The vertical position of the note, from bottom to top [0..2]
    speedOK: boolean; // Cut speed was fast enough
    directionOK: null | boolean; // Note was cut in the correct direction. null for bombs.
    saberTypeOK: null | boolean; // Note was cut with the correct saber. null for bombs.
    wasCutTooSoon: boolean; // Note was cut too early
  };
}
