import { Colour, PhilipsTV } from "@edwardsalter/philipsandroidtv";
import { NoteType } from "./types/NoteCutEvent";

const RED = { r: 255, g: 0, b: 0 };
const BLUE = { r: 0, g: 0, b: 255 };
const BLACK = { r: 0, g: 0, b: 0 };

class TV {
  private tv: PhilipsTV;
  private lastLight: Partial<Record<NoteType, number>> = {};
  private timeouts: Partial<Record<NoteType, ReturnType<typeof setTimeout>>> =
    {};

  constructor() {
    const ip = process.env.TV_IP;
    const user = process.env.TV_USER;
    const pass = process.env.TV_PASS;

    if (!ip || !user || !pass)
      throw new Error(
        "Unknown TV IP or username/password. Ensure TV_IP, TV_USER and TV_PASS envrionment variables are set."
      );

    this.tv = new PhilipsTV(ip, undefined, {
      user,
      pass,
      sendImmediately: false,
    });
  }

  public setLight(noteType: NoteType, layer: number, line: number) {
    if (noteType !== "NoteA" && noteType !== "NoteB") return;
    const lightNum = TV.mapColumnToLightNum(line);

    this.tv.setAmbilightColours({
      layer1: {
        top: this.createLightSetter(lightNum, noteType),
        left: BLACK,
        right: BLACK,
      },
    });
    this.setTimerToClearLight(noteType);
  }

  private setTimerToClearLight(noteType: NoteType) {
    const prevTimeout = this.timeouts[noteType];
    if (prevTimeout) {
      clearTimeout(prevTimeout);
    }

    this.timeouts[noteType] = setTimeout(() => {
      this.tv.setAmbilightColours({
        layer1: {
          top: this.createLightSetter(undefined, noteType),
        },
      });
    }, 100);
  }

  private createLightSetter(index: number | undefined, noteType: NoteType) {
    const obj: Record<number, Colour> = {};

    // Clear the previous light
    const prevLight = this.lastLight[noteType];
    if (prevLight != null) obj[prevLight] = BLACK;

    // Set the new light and keep track of which index was set
    if (index) {
      obj[index] = noteType === "NoteA" ? RED : BLUE;
    }
    this.lastLight[noteType] = index;

    return obj;
  }

  private static mapColumnToLightNum(column: number) {
    // TODO: SHOULD PROBABLY BE CHECKING HOW MANY LIGHTS WE HAVE FIRST
    switch (column) {
      case 0:
        return 0;
      case 1:
        return 2;
      case 2:
        return 4;
      case 3:
        return 6;
      default:
        throw new Error("Received unknown column in mapColumnToLightNum");
    }
  }
}

export default TV;
