import dotevnv from "dotenv";
import WebSocket from "ws";
import { NoteCutEvent } from "./types/NoteCutEvent";
import TV from "./tv";

dotevnv.config();

const tv = new TV();

const ws = new WebSocket("ws://localhost:6557/socket");

ws.on("close", function close(code, reason) {
  console.log("disconnected", code, reason.toString());
});

ws.on("open", function open() {
  console.log("on open");
});

ws.on("message", function incoming(message) {
  const string = message.toString();

  const { event, noteCut } = JSON.parse(string) as NoteCutEvent;
  if (event !== "noteCut") return;

  tv.setLight(noteCut.noteType, noteCut.noteLayer, noteCut.noteLine);
});

process.on("SIGINT", function () {
  ws.close();
});
